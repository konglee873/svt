//
//  AppDelegate.swift
//  SVTHelper
//
//  Created by konglee on 2020/2/23.
//  Copyright © 2020 konglee. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
        let workspace = NSWorkspace.shared
        var svtLaunched = workspace.launchApplication("/Applications/SVT.app")
        if !svtLaunched {
            svtLaunched = workspace.launchApplication("SVT.app")
        }
        if !svtLaunched {
            let path = Bundle.main.bundlePath as NSString
        
            let ld = max(path.pathComponents.count - 4, 0)
            let pathComponents = path.pathComponents[0...ld]
            let subPath = NSString.path(withComponents: Array(pathComponents))
            workspace.launchApplication(subPath)
        }
        NSApp.terminate(nil)
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

