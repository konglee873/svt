//
//  RequestAccess.swift
//  SVT
//
//  Created by konglee on 2020/2/23.
//  Copyright © 2020 konglee. All rights reserved.
//

import Cocoa
import ServiceManagement

class RequestAccess {
    private let accessKey = "SVTCAccessKey"
    private let accessID = "com.kong.SVT.SVTHelper"
    
    static let sharedInstance = RequestAccess()
    fileprivate(set) var didAllowAccess: Bool = false
    
    private init() {}
    
    func requestForAccess() {
        didAllowAccess = UserDefaults.standard.bool(forKey: accessKey)
        if !didAllowAccess {
            access()
        }
    }
    
    private func access() {
        if SMLoginItemSetEnabled(accessID as CFString, didAllowAccess) {
            didAllowAccess = true
            UserDefaults.standard.set(true, forKey: accessKey)
        }
    }

}
